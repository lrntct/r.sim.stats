#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
MODULE:    r.sim.stats

AUTHOR(S): Laurent Courty

PURPOSE:    Calculate indices for evaluating forecasting models by
            comparing forecasted and observed event.

COPYRIGHT: (C) 2016 by Laurent Courty

            This program is free software; you can redistribute it and/or
            modify it under the terms of the GNU General Public License
            as published by the Free Software Foundation; either version 2
            of the License, or (at your option) any later version.

            This program is distributed in the hope that it will be useful,
            but WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
            GNU General Public License for more details.
"""

#%option G_OPT_R_INPUT
#% key: forecast
#% description: Input forecasted binary map
#% required: yes
#%end

#%option G_OPT_R_INPUT
#% key: observed
#% description: Input observed binary map
#% required: yes
#%end

#%flag
#% key: g
#% description: Print results in shell style
#%end

from __future__ import division
import sys
import os
import grass.script as grass
from grass.pygrass.messages import Messenger


class Stats(object):
    """Provide methods to calculate and print statistics
    """
    def __init__(self, cell_count):
        self.cell_count = cell_count
        self.hits = cell_count['hits']
        self.fa = cell_count['false_alarms']
        self.misses = cell_count['misses']
        self.c_neg = cell_count['correct_neg']
        self.cell_sum = sum(cell_count.values())
        self.stats = {'accuracy': [None, u"Accuracy"],
                      'bias': [None, u"Bias score"],
                      'pod': [None, u"Probability of detection"],
                      'far': [None, u"False alarm ratio"],
                      'pofd': [None, u"Probability of false detection"],
                      'sr': [None, u"Success ratio"],
                      'ts': [None, u"Threat score"],
                      'ets': [None, u"Equitable threat score"],
                      'hk': [None, u"Hanssen & Kuipers discriminant"],
                      'hss': [None, u"Heidke skill score"],
                      'or': [None, u"Odds ratio"],
                      'orss': [None, u"Odds ratio skill score"]}

    def calculate(self):
        """Calculate statistics and store them in a dictionary
        """
        self.stats['accuracy'][0] = (self.hits + self.c_neg) / self.cell_sum
        self.stats['bias'][0] = ((self.hits + self.fa) /
                                 (self.hits + self.misses))
        self.stats['pod'][0] = self.hits / (self.hits + self.misses)
        self.stats['far'][0] = self.fa / (self.hits + self.fa)
        self.stats['pofd'][0] = self.fa / (self.c_neg + self.fa)
        self.stats['sr'][0] = self.hits / (self.hits + self.fa)
        self.stats['ts'][0] = self.hits / (self.hits + self.misses + self.fa)
        hits_rd = (((self.hits + self.misses) *
                   (self.hits + self.fa)) / self.cell_sum)
        self.stats['ets'][0] = ((self.hits - hits_rd) /
                                (self.hits + self.misses + self.fa - hits_rd))
        self.stats['hk'][0] = ((self.hits / (self.hits + self.misses)) -
                               (self.fa / (self.c_neg + self.fa)))
        ec_rd = (1 / self.cell_sum) * ((self.hits + self.misses) *
                                       (self.hits + self.fa) +
                                       (self.c_neg + self.misses) *
                                       (self.c_neg + self.fa))
        self.stats['hss'][0] = (((self.hits + self.c_neg) - ec_rd) /
                                (self.cell_sum - ec_rd))
        self.stats['or'][0] = (self.hits * self.c_neg) / (self.misses * self.fa)
        self.stats['orss'][0] = (((self.hits * self.c_neg) -
                                  (self.misses * self.fa)) /
                                 ((self.hits * self.c_neg) +
                                  (self.misses * self.fa)))

    def print_results(self):
        """Print results in a human readable way
        """
        print(u"{:>30}: {}".format(u"Hits", self.hits))
        print(u"{:>30}: {}".format(u"Misses", self.misses))
        print(u"{:>30}: {}".format(u"False alarms", self.fa))
        print(u"{:>30}: {}".format(u"Correct negatives", self.c_neg))
        for v in self.stats.values():
            print(u"{:>30}: {:.3f}".format(v[1], v[0]))

    def print_results_shell(self):
        """Print results in shell style
        """
        for k, v in self.cell_count.iteritems():
            print(u"{}={}".format(k, v))
        for k, v in self.stats.iteritems():
            print(u"{}={}".format(k, v[0]))


def main():
    # start messenger
    msgr = Messenger()

    rast_forecast = options['forecast']
    rast_observed = options['observed']

    # set default to 0, in case one category is absent in results
    cell_count = {'hits': 0, 'false_alarms': 0,
                  'misses': 0, 'correct_neg': 0}

    input_maps = rast_forecast + ',' + rast_observed
    res = grass.read_command("r.stats", flags="cn", input=input_maps)

    # classify
    for line in res.strip().split(os.linesep):
        line = line.split()
        # line[0] = forecast, line[1] = observed
        bool_hits =         (line[0] == '1' and line[1] == '1')
        bool_false_alarms = (line[0] == '1' and line[1] == '0')
        bool_misses =       (line[0] == '0' and line[1] == '1')
        bool_correct_neg =  (line[0] == '0' and line[1] == '0')
        if bool_hits:
            cell_count['hits'] = int(line[2])
        elif bool_false_alarms:
            cell_count['false_alarms'] = int(line[2])
        elif bool_misses:
            cell_count['misses'] = int(line[2])
        elif bool_correct_neg:
            cell_count['correct_neg'] = int(line[2])
        else:
            msgr.fatal(_(u"Unknown posibility: '{}, {}'. "
                         u"Please provide binary maps (0 or 1)".format(line[0],
                                                                       line[1])))

    stats = Stats(cell_count)
    stats.calculate()
    if flags['g']:
        stats.print_results_shell()
    else:
        stats.print_results()


if __name__ == "__main__":
    options, flags = grass.parser()
    sys.exit(main())
