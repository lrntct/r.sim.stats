#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (C) 2015  Laurent Courty

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
"""
import unittest

# black magic to import the main file as module, even with invalid name
import imp
with open('r.sim.stats.py', 'rb') as fp:
    stats = imp.load_module(
        'stats', fp, 'models.admin.py',
        ('.py', 'rb', imp.PY_SOURCE))
Stats = stats.Stats

class TestStats(unittest.TestCase):
    """Tests results using values from:
    http://www.cawcr.gov.au/projects/verification/index.php
    """
    @classmethod
    def setUpClass(cls):
        """create test data"""
        # instantiate the class to be tested
        test_data = {'hits':82, 'false_alarms':38,
                'misses':23, 'correct_neg':222}
        cls.stats = Stats(test_data)
        cls.stats.calculate()

    @classmethod
    def tearDownClass(cls):
        pass

    def test_init(self):
        """Test if the initialization is done properly
        """
        self.assertEqual(self.stats.hits, 82)
        self.assertEqual(self.stats.fa, 38)
        self.assertEqual(self.stats.misses, 23)
        self.assertEqual(self.stats.c_neg, 222)

    def test_stats(self):
        """
        """
        self.assertAlmostEqual(self.stats.stats['accuracy'][0], .83, places=2)
        self.assertAlmostEqual(self.stats.stats['bias'][0], 1.14, places=2)
        self.assertAlmostEqual(self.stats.stats['pod'][0], .78, places=2)
        self.assertAlmostEqual(self.stats.stats['far'][0], .32, places=2)
        self.assertAlmostEqual(self.stats.stats['pofd'][0], .15, places=2)
        self.assertAlmostEqual(self.stats.stats['sr'][0], .68, places=2)
        self.assertAlmostEqual(self.stats.stats['ts'][0], .57, places=2)
        self.assertAlmostEqual(self.stats.stats['ets'][0], .44, places=2)
        self.assertAlmostEqual(self.stats.stats['hk'][0], .63, places=2)
        self.assertAlmostEqual(self.stats.stats['hss'][0], .61, places=2)
        self.assertAlmostEqual(self.stats.stats['or'][0], 20.8, places=1)
        self.assertAlmostEqual(self.stats.stats['orss'][0], .91, places=2)


if __name__ == '__main__':
    unittest.main()
