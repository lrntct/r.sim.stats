## DESCRIPTION

_r.sim.stats_ computes and prints a series of scores used to evaluate the results of an environmental model by comparing them to fields measurements. It is based on methods described by Stanski et al.[0] and compiled by the Collaboration for Australian Weather and Climate Research[1]

_r.sim.stats_ takes two raster maps as input. Both should be in the form of binary data:

*   0 means that the event is _not_ forecast/observed
*   1 means that the event _is_ forecast/observed
*   NULL values are ignored

The contingency table below shows the relation between values of both maps. Those values are also printed in the module output.

|             | Observed | Not observed          |
| ----------- | -------- | --------------------- |
| Forecast    | Hits     | False alarms          |
| Not forecast| Misses   | Correct negatives     |


The calculated scores are described below.

### Accuracy

AKA Percent Correct

Range: 0 to 1

No skill: 0

Perfect score: 1

### Bias score

Range: 0 to ∞

No skill: 0

Perfect score: 1

### Probability of detection

AKA hit rate

Range: 0 to 1

No skill: 0

Perfect score: 1

### False alarm ratio

Range: 0 to 1

No skill: 1

Perfect score: 0

### Probability of false detection

AKA false alarm rate

Range: 0 to 1

No skill: 1

Perfect score: 0

### Success ratio

Range: 0 to 1

No skill: 0

Perfect score: 1

### Threat score

AKA critical success index

Range: 0 to 1

No skill: 0

Perfect score: 1

### Equitable threat score

AKA Gilbert skill score

Range: -1/3 to 1

No skill: 0

Perfect score: 1

### Hanssen & Kuipers discriminant

AKA true skill statistic, Peirce's skill score

Range: -1 to 1

No skill: 0

Perfect score: 1

### Heidke skill score

AKA Cohen's k

Range: -1 to 1

No skill: 0

Perfect score: 1

### Odds ratio

Range: 0 to ∞

No skill: 1

Perfect score: ∞

### Odds ratio skill score

AKA Yule's Q

Range: -1 to 1

No skill: 0

Perfect score: 1

## NOTE

Although the computed scores are originally designed to evaluate climate forecasting models, they could be used for other kind of models. The values of the above-mentioned contingency table are obtained by a underlying call to _r.stats_

## REFERENCES

[0] Stanski, H.R., Wilson, L.J. & Burrows, W.R., 1989\. Survey of common verification methods in meteorology, World Meteorological Organization Geneva. Available at: http://www.cawcr.gov.au/projects/verification/Stanski_et_al/Stanski_et_al.html.

[1] [Verification methods for dichotomous forecasts](http://www.cawcr.gov.au/projects/verification/index.php#Methods_for_dichotomous_forecasts)

## SEE ALSO

r.stats

## AUTHOR

Laurent G. Courty, Instituto de Ingeniería UNAM, Mexico
